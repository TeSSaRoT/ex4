package com.example.itryex4

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.itryex4.databinding.ActivityMapsBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private val gomel = LatLng(52.43138332938815, 30.99392858427311)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, 10.0F))

        val belarusbankAtmAPIService = BelarusbankAtmAPIService.create()
        belarusbankAtmAPIService.getAtmByCity(resources.getString(R.string.gomel))
            ?.enqueue(object : Callback<List<BelarusbankATM?>?> {
                override fun onResponse(
                    call: Call<List<BelarusbankATM?>?>,
                    response: Response<List<BelarusbankATM?>?>
                ) {
                    if (response.body().isNullOrEmpty()) {
                        Toast.makeText(this@MapsActivity, R.string.nullResponce, Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        response.body()?.forEach { which ->
                            if (which != null) {
                                mMap.addMarker(
                                    MarkerOptions().position(
                                        LatLng(
                                            which.gps_x,
                                            which.gps_y
                                        )
                                    )
                                )
                            }
                        }
                    }
                }
                override fun onFailure(call: Call<List<BelarusbankATM?>?>, t: Throwable) {
                    Toast.makeText(this@MapsActivity, R.string.failResponce, Toast.LENGTH_SHORT)
                        .show()
                }
            })
    }
}