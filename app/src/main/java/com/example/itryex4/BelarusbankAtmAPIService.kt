package com.example.itryex4

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

const val baseUrl = "https://belarusbank.by/api/"

interface BelarusbankAtmAPIService {

    @GET("atm")
    fun getAtmByCity(@Query("city") city: String): Call<List<BelarusbankATM?>?>?

    companion object Factory {
        fun create(): BelarusbankAtmAPIService {

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .build()

            return retrofit.create(BelarusbankAtmAPIService::class.java)
        }
    }
}