package com.example.itryex4

import com.google.gson.annotations.SerializedName

data class BelarusbankATM(

    @SerializedName("city") val city: String,
    @SerializedName("gps_x") val gps_x: Double,
    @SerializedName("gps_y") val gps_y: Double,
)